﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GetionCursos.Web.Startup))]
namespace GetionCursos.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
